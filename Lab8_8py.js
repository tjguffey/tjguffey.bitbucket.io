var Lab8_8py =
[
    [ "count_isr", "Lab8_8py.html#a0ff3a64c98df93db256baa141a72c52d", null ],
    [ "extint", "Lab8_8py.html#a289a2527c92bbc76b43785b0b3de25e5", null ],
    [ "fault", "Lab8_8py.html#a49b3df8b04cbdb67a904be22eabd6d80", null ],
    [ "faultADC", "Lab8_8py.html#a835c13a32da4a0131aaedf7f7bbb9fbf", null ],
    [ "isr_count", "Lab8_8py.html#a2262f30a1a25c811168737eca3aa0b85", null ],
    [ "mode", "Lab8_8py.html#aa068a42a01a7d0e2c844590710d22217", null ],
    [ "moeb", "Lab8_8py.html#ab0e2127f0eb8ea83aa34ced16d76dbed", null ],
    [ "moet", "Lab8_8py.html#aa8a5e9b850a3e57fbde546a93a2c1151", null ],
    [ "nfault", "Lab8_8py.html#aa79d747e88d8fabe3d69ccb9844dc78a", null ],
    [ "pin_IN1", "Lab8_8py.html#ae08b0c2b3109bcb0e82e0c13e1b22886", null ],
    [ "pin_IN12", "Lab8_8py.html#a4fc0e86f158a461a616a353aed706412", null ],
    [ "pin_IN2", "Lab8_8py.html#aa777c00b7ad2435a5e7ccb5f1447418c", null ],
    [ "pin_IN22", "Lab8_8py.html#a4d42be9238f60ed2d5b3e17afc17cf35", null ],
    [ "pin_nSLEEP", "Lab8_8py.html#a8aa679f5328b264f1fc74655d3e0747f", null ],
    [ "tim1", "Lab8_8py.html#aaaea8dbb318eb659b027eaa25403fcd4", null ],
    [ "tim2", "Lab8_8py.html#a034b645f9d2bd51db9e6fe4233a3ad30", null ]
];