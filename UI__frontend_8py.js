var UI__frontend_8py =
[
    [ "plot4u", "UI__frontend_8py.html#a69c9b0b70e32d69493ffa0f33e115320", null ],
    [ "sendChar", "UI__frontend_8py.html#ae105b442594380fc898124959890b340", null ],
    [ "conv", "UI__frontend_8py.html#a983ae44ccda19dc62953a88855b2becf", null ],
    [ "conv2", "UI__frontend_8py.html#a22ff8ada3cf2c81b4887a31f62be8d34", null ],
    [ "current_time", "UI__frontend_8py.html#a78a35ef58d41aedcbf3051445a43edac", null ],
    [ "data_list", "UI__frontend_8py.html#aab0f9b080cf19cae669267998211e52d", null ],
    [ "data_points", "UI__frontend_8py.html#aff357ff33d440d4302dc288d88f7df88", null ],
    [ "file", "UI__frontend_8py.html#a9b72d8990c01390147d3653377161477", null ],
    [ "floated", "UI__frontend_8py.html#a73f11f649ae7da686d6bec11bd84c37f", null ],
    [ "frequency", "UI__frontend_8py.html#ae6b05391c2d00b23ba57ababac8a0be7", null ],
    [ "lag_out", "UI__frontend_8py.html#a62141dcc0b7ab1d70c6a65bd88d33b93", null ],
    [ "myList", "UI__frontend_8py.html#ab67f6a463f348d8f3493259fb120e18d", null ],
    [ "myRows", "UI__frontend_8py.html#a1800db22f9aa55a7b449bad06d6e839f", null ],
    [ "results", "UI__frontend_8py.html#a9e36bf9b8e960a4d503043b48c855f97", null ],
    [ "ser", "UI__frontend_8py.html#a54aa971a38876c2c4f7f3da2e6b46265", null ],
    [ "start", "UI__frontend_8py.html#ae42ff87ee93cbe984dc82d12b8659acf", null ],
    [ "time_list", "UI__frontend_8py.html#a0973f6ddcab7fb06324c2de6189812b0", null ],
    [ "times", "UI__frontend_8py.html#ae137dea4463e0c2d0d0565711190cb92", null ],
    [ "times_index", "UI__frontend_8py.html#af43262d59c3467135b6e75cb946993d3", null ],
    [ "wait_time", "UI__frontend_8py.html#a5998237184fff38af3d0a789c98bd799", null ],
    [ "writer", "UI__frontend_8py.html#a338e885bcfdfd9823be5421b2b276f91", null ]
];