var files_dup =
[
    [ "Encoder.py", "Encoder_8py.html", "Encoder_8py" ],
    [ "lab0x00.py", "lab0x00_8py.html", null ],
    [ "Lab0x01.py", "Lab0x01_8py.html", "Lab0x01_8py" ],
    [ "Lab0x02.py", "Lab0x02_8py.html", [
      [ "realblink", "classLab0x02_1_1realblink.html", "classLab0x02_1_1realblink" ]
    ] ],
    [ "lab7.py", "lab7_8py.html", "lab7_8py" ],
    [ "Lab8.py", "Lab8_8py.html", "Lab8_8py" ],
    [ "Lab9.py", "Lab9_8py.html", [
      [ "BalanceBall", "classLab9_1_1BalanceBall.html", "classLab9_1_1BalanceBall" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main4.py", "main4_8py.html", "main4_8py" ],
    [ "mainADC.py", "mainADC_8py.html", "mainADC_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "mcp", "classmcp9808_1_1mcp.html", "classmcp9808_1_1mcp" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "plotfile.py", "plotfile_8py.html", "plotfile_8py" ],
    [ "TouchScreen.py", "TouchScreen_8py.html", "TouchScreen_8py" ],
    [ "UI_frontend.py", "UI__frontend_8py.html", "UI__frontend_8py" ]
];