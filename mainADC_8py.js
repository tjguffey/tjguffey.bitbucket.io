var mainADC_8py =
[
    [ "adc", "mainADC_8py.html#a90765aa6cad8fdaf990c145bffefd399", null ],
    [ "buf", "mainADC_8py.html#a4e7e190a52249efdb05eafe2ee87b38f", null ],
    [ "conv", "mainADC_8py.html#a0ac0f8508c6143f142d3d0ab4b443932", null ],
    [ "data_points", "mainADC_8py.html#a862d921d7e8d5f38bced66924cfd9595", null ],
    [ "frequency", "mainADC_8py.html#a076da0166024737a3efdd062957b20d7", null ],
    [ "good_trans", "mainADC_8py.html#a7d9502da33f3ac489c063d5acd2f606f", null ],
    [ "maximum", "mainADC_8py.html#a250b058ebac8e01477536f7cb1c35dfc", null ],
    [ "minimum", "mainADC_8py.html#a63044ec1db10c2d1376e96054fac79d3", null ],
    [ "mock_sensor", "mainADC_8py.html#a354ff7382d7cbfcedf35703b78233a8d", null ],
    [ "myuart", "mainADC_8py.html#a436a10abc8182ff7b32e4fcd089be8e1", null ],
    [ "tim", "mainADC_8py.html#a8d83e25a397f0eaedc7587d3dbd99ad5", null ],
    [ "val", "mainADC_8py.html#aa976fb331b6cdf60485cc7ca9bddff7e", null ]
];